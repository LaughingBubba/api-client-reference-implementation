# DNAiQ API Documentation

## Sequencing

![Sequence](./assets/DNAiQ API Sequence Diagram v1.0.png)

### 1) Notification From LIMS

When a genotyping run has been completed, the Laboratory Information Management System (LIMS) notifies the API Server of the `runID` that has been completed.

### 2) Notification POSTed to Client

**(1)** Will then trigger the API Server to assess which registered API clients are affected by the run and then notify each registered Client in turn.

The API Client will be notified via a HTTP POST to the URL specified in the "API Client (Web) Admin". The body of the HTTP POST request will havea JSON string with the `runID` and the `getURL` which is the URL the API Client will need to GET the results.

Example:
```JSON
{
  "runID": "abc123",
  "getURL": "https://api.dnaiq.com/results/run/abc123"
}
```

The `POST Secret` specified by the API Client (Web) Admin will also be provided in the HTTP Authorization Header as a Bearer Token to ensure the **POST** request is from a trusted source.

### 3) Client Requests GET of Results

When the API Client receives the notification, it issues a HTTP GET to the API Server via the URL provided in **(2)**.

The `GET Secret` specified by the API Client (Web) Admin will also be provided in the HTTP Authorization Header as a Bearer Token to ensure the **GET** request is from a trusted source.

### 4) API Server JSON Respose to Client

The response to the GET **(3)** is JSON payload of the results.

Example:
```JSON
{
  "results":
  [
    {"sampleID":"12345","rs":"rs143383","A1":"G","A2":"G"},
    {"sampleID":"12345","rs":"rs17366568","A1":"G","A2":"G"},
    {"sampleID":"12345","rs":"rs1801394","A1":"A","A2":"G"},
    {"sampleID":"12345","rs":"rs1048943","A1":"T","A2":"T"},
  ]
}
```

HTTP Codes:
- 201: OK, resource created -> JSON payload is in the HTTP body.
- 401: Unauthorised -> Invalid JWT in Authorization Header, please check API Client (Web) Admin page.
- 404: Not found -> runID in the URL doesnt exist.
- 421: Misdirected Request -> Malformed URL.


### 5) API Client Repsonse to Notification

The API Clients' response to the initial notification **(1)** will be based on the the returned HTTP code from **(4)**.

HTTP Codes:
- 200: OK -> 201 received 
- 4xx: Echo -> Return the non-201 code received in **(4)**.

### 6) API Server JSON Respose to LIMS

Once all API Client responses have been received, a summary of the outcomes is returned to the LIMS.

Example:
```JSON
{
  "runID": "abc123",
  "clientsNotified": 1,
  "errors": []
}
```

HTTP Codes:
- 200: OK -> Will always be OK, JSON payload in the HTTP body will describe success/failure of notifications. 

## Secure Routes using Bearer Tokens

POST's from the API Server to the Client will always container a Bearer Token in the HTTP Authorization Header 

## API Client (Web) Admin

Registered API Clients can update their URL and API secret at : [https://api.dnaiq.com/](https://api.dnaiq.com/).

Login details will be provided by DNAiQ.

Note: Only POST URL and POST Secret can be edited by the Client. GET Secret (JWT) is defined by DNAiQ.

## API Client Reference Implementation

A [reference implementation](https://gitlab.com/dnaiq/api-client-reference-implementation) has been created using PHP and the slimPHP 4 framework. This was used to test the API Server interaction.

Clients are welcome to fork this Repo and adapt to their needs. 

NOTE: No warranty is expressed or impled and is offered on an "as is" basis.